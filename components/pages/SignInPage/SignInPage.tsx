'use client';

import { signIn } from 'next-auth/react';

const SignInPage = () => <button onClick={() => signIn('google')}>Zaloguj za pomocą Google</button>;

export default SignInPage;
