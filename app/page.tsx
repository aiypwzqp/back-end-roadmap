'use client';

import { signIn, signOut, useSession } from 'next-auth/react';

import Button from '@/atoms/Button';

export default function Home() {
  const { data: session } = useSession();

  return (
    <main>
      <Button />
      <h1>Strona główna</h1>
      {session && session.user ? (
        <button onClick={() => signOut()}>Wyloguj</button>
      ) : (
        <button onClick={() => signIn()}>Zaloguj</button>
      )}
    </main>
  );
}
